<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8"/>
    <title>PHP array to JS object</title>
  </head>
  <body>
    <h1>Load Data</h1>
    <button id="load">Load</button>
    <script>
     <?php 
     $data = array(
       "first name" => "Shafiqur",
       "lastname" => "Rahman"
     );
     ?>
     document.getElementById("load").addEventListener(
       "click", function(){
	 let personObj = <?= json_encode($data); ?>;
	 console.log(personObj['first name']);
	 console.log(personObj.lastname);
     });
    </script>
  </body>
</html>

<?php

function loadData(){
  return array(
    array(
      'id' => 1,
      'fname'=>'Abul',
      'lname'=>'kabul',
      'age'=>'10',
      'roll'=>'01',
    ),
    array(
      'id' => 2,
      'fname'=>'Babul',
      'lname'=>'Dabul',
      'age'=>'11',
      'roll'=>'02',
    ),
    array(
      'id' => 3,
      'fname'=>'Cabul',
      'lname'=>'Mabul',
      'age'=>'11',
      'roll'=>'03',
    ),
    array(
      'id' => 4,
      'fname'=>'Labul',
      'lname'=>'Mabul',
      'age'=>'10',
      'roll'=>'04',
    ),
    array(
      'id' => 5,
      'fname'=>'Kabul',
      'lname'=>'Vabul',
      'age'=>'11',
      'roll'=>'05',
    ),
    array(
      'id' => 6,
      'fname'=>'Gabul',
      'lname'=>'Habul',
      'age'=>'10',
      'roll'=>'06',
    )
  );
}

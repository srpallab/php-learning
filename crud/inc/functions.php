<?php
session_name("CURD");
session_start([
  'cookie_lifetime' => 60000
]);

$currentPath = getcwd();
// echo $currentPath;
// die;

define('DB_NAME', $currentPath . '/data/db.txt');
define('USERS', $currentPath . '/data/users.txt');

// Reading Data From File and return Array
function rdffara(){
  // Reading Serialized Data
  $ssd = file_get_contents(DB_NAME);
  // Unserializing the data
  $sdu = unserialize($ssd);
  // print_r($sdu);
  return $sdu;
}

// Serialize Array and Writing Data
function saawd($data){
  // Serialize Data From File
  $sdff = serialize($data);
  // echo $sd;
  file_put_contents(DB_NAME, $sdff, LOCK_EX);
}

// Finding the Max id in the array
function ftmiita($data){
  // print_r($data);
  $maxId = max(array_column($data, 'id'));
  echo $maxId;
  return $maxId + 1;
}

// Cheacking User Permission and Setting Roles
function hasAccess($role){
  // echo $role;
  if ('admin' == $role || 'editor' == $role ) {
    return true;
  }
  return false;
}

// Log User In
function userLogin($uname, $pwd){
  $handler = fopen(USERS, "r");
  while ($data = fgetcsv($handler)) {
    // print_r($data);
    if($data[0] == $uname && $data[1] == sha1($pwd)){
      return $data;
    }
  }
  return false;
}

// Add New Data to File
function addNewData($fname, $lname, $age, $roll){
  $found = false;
  $studentAdd = rdffara();
  $newId = ftmiita($studentAdd);
  $newStudent = array(
    'id'    => $newId,
    'fname' => $fname,
    'lname' => $lname,
    'age'   => $age,
    'roll'  => $roll
  );
  foreach($studentAdd as $st){
    if($st['roll'] == $newStudent['roll']){
      $found = true;
      return $newStudent;
    }
  }
  array_push($studentAdd, $newStudent);
  saawd($studentAdd);
  header('Location: index.php?task=report&info=New+Student+Added');
  return $found;
}


// Find a Student
function findOldData($ido){
  $studentOld = rdffara();
  foreach($studentOld as $st){
    if($st['id'] == $ido){
      //echo $st['id'];
      //echo $ido;
      return $st;
    }
  }
}

// Find a Student Index
function findOldDataIndex($ido){
  $studentOld = rdffara();
  $ind = 0;
  foreach($studentOld as $st){
    if($st['id'] == $ido){
      return $ind;
    }
    $ind++;
  }
}


// Edit An Student
function editData($ido, $fname, $lname, $age, $roll){
  $studentEdit = rdffara();
  $found = false;
  foreach($studentEdit as $st){
    if($st['roll'] == $roll && $st['id'] != $ido){
      $found = true;
      // echo $ido;
      return array(
	'id'    => $ido,
	'fname' => $fname,
	'lname' => $lname,
	'age'   => $age,
	'roll'  => $roll
      );
    }
  }
  $oii = findOldDataIndex($ido);
  $studentEdit[$oii]['id'] = $ido;
  $studentEdit[$oii]['fname'] = $fname;
  $studentEdit[$oii]['lname'] = $lname;
  $studentEdit[$oii]['age'] = $age;
  $studentEdit[$oii]['roll'] = $roll;
  saawd($studentEdit);
  header('Location: index.php?task=report&info=Student+Updated');
  return $found;
}

// Delete Data From File
function deleteOldData($ido){
  $studentsAll = rdffara();
  $inde = findOldDataIndex($ido);
  unset($studentsAll[$inde]);
  saawd($studentsAll);
  header('Location: index.php?task=report&info=Student+Deleted');
}


// Loading Dummy Data 
function seed(){
  require_once "data.inc.php";
  $students = loadData();
  // print_r($students);
  saawd($students);
}


// Generate a Table of Students
function reportGenerate($role){
  $students = rdffara();
  $html = "<table>";
  $html .= "<tr>";
  $html .= "<th>Full Name</th>";
  $html .= "<th>Age</th>";
  $html .= "<th>Roll</th>";
  if (hasAccess($role)) {
    $html .= "<th>Actions</th>";
  }
  $html .= "</tr>";
  foreach($students as $student){
    $html .= "<tr>";
    $html .= "<td>{$student['fname']} {$student['lname']}</td>";
    $html .= "<td>{$student['age']}</td>";
    $html .= "<td>{$student['roll']}</td>";
    $html .= "<td> ";
    if (hasAccess($role)){
      $html .= "<a href='index.php?";
      $html .= "id={$student['id']}&task=edit'>Edit</a> ";
      if ('admin' == $role) {
        $html .= "| <a class='delete' ";
        $html .= "href='index.php?id={$student['id']}&task=delete'>";
        $html .= "Delete</a>";
      }
      $html .= "</td>";
    }
    $html .= "</tr>";
  }
  $html .= "</table>";
  echo $html;
}



?>

<div class="navigation">
  <p style="float:left;">
    <a href="index.php?task=report">All Students</a>
    <?php if(isset($_SESSION['user']) && hasAccess($_SESSION['role'])): ?>
      | <a href="index.php?task=add">Add Students</a>
      <?php if('admin' == $_SESSION['role']): ?>
	| <a href="index.php?task=seed">Seed</a>
      <?php endif;  ?>
    <?php endif; ?>
  </p>
  <p style="float:right">
    <?php if(isset($_SESSION['user']) && $_SESSION['user'] != false): ?>
      <a href="index.php?task=logout">Logout (<?= $_SESSION['role']; ?>)</a>
    <?php else: ?>
      <a href="index.php?task=login">Login</a>
    <?php endif; ?>
  </p>
</div>

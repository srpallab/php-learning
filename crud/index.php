<?php

require_once "inc/functions.php";
$info = $_GET['info'] ?? "";
$task = $_GET['task'] ?? 'report';
if(isset($_GET['task'])){
  if('seed' == $_GET['task']){
    // echo DB_NAME;
    if(hasAccess($_SESSION['role'])){
      seed();
      $info = "Seeding Complete!"; 
    }else{
      $info = "You need to Log In First!";
    }
  }else if('add' == $_GET['task']){
    if(isset($_POST['submit'])){
      $fname = filter_input(INPUT_POST, 'fname', FILTER_SANITIZE_STRING);
      $lname = filter_input(INPUT_POST, 'lname', FILTER_SANITIZE_STRING);
      $age = filter_input(INPUT_POST, 'age', FILTER_SANITIZE_STRING);
      $roll = filter_input(INPUT_POST, 'roll', FILTER_SANITIZE_STRING);
      if($fname != "" && $lname != "" && $age != "" && $roll != ""){
	$isAddedOrData = addNewData($fname, $lname, $age, $roll);
	if($isAddedOrData != false){
	  $info = "Duplicate Roll!";
	}
      }else{
	$info = "Value Can't be null!";
      }
    }

  }else if('edit' == $_GET['task']){
    if(!hasAccess($_SESSION['role'])){
      header('Location: index.php?task=report');
      return;
    }
    if(isset($_GET['id'])){
      $isAddedOrData = findOldData($_GET['id']);
    }
    if(isset($_POST['update'])){
      $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
      $fname = filter_input(INPUT_POST, 'fname', FILTER_SANITIZE_STRING);
      $lname = filter_input(INPUT_POST, 'lname', FILTER_SANITIZE_STRING);
      $age = filter_input(INPUT_POST, 'age', FILTER_SANITIZE_STRING);
      $roll = filter_input(INPUT_POST, 'roll', FILTER_SANITIZE_STRING);
      if($id != "" && $fname != "" && $lname != "" && $age != "" && $roll != ""){
	$isAddedOrData = editData($id, $fname, $lname, $age, $roll);
	if($isAddedOrData != false){
	  $info = "Duplicate Roll!";
	}else{
	  $info = "Student Updated!";
	}
      }else{
	$info = "Value Can't be null!";
      }
    }
  }else if('delete' == $_GET['task']){
    if(hasAccess($_SESSION['role']) && 'admin' == $_SESSION['role']){
      if(isset($_GET['id'])){
	deleteOldData($_GET['id']);
      } 
    }else{
      $info = "You need to Log In First!";
    }
  } else if('login' == $_GET['task']){
    if(isset($_POST['login'])){
      $uname = filter_input(INPUT_POST, 'uname', FILTER_SANITIZE_STRING);
      $pwd = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
      if($uname != "" && $pwd != ""){
	$user = userLogin($uname, $pwd);
	if(!$user){
	  $info = "Username or Password is Wrong!";
	}else{
	  $_SESSION['user'] = $user[0];
	  $_SESSION['role'] = $user[2];
	  header('Location: index.php?task=report');
 	}
      }
    }
  } else if('logout' == $_GET['task']){
    $_SESSION['user'] = false;
    $_SESSION['role'] = false;
    header('Location: index.php?task=report');
    
  }
}
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8"/>
    <title>Document</title>
    <link href="assets/css/style.css" rel="stylesheet"/>
  </head>
  <body>
    <div class="container">
      <div class="row">
	<div class="col">
	  <h2>Project 2 CRUD</h2>
	  <p>
	    A Simple Project to Perform CRUD Operation using Plain File and PHP
	  </p>
	  <?php include_once ("inc/templates/nav.php"); ?>
	  <?php if ($info != "") :?>
	    <p style="color:red;"><?= $info; ?></p>
	  <?php endif; ?>
	</div>
	<div class="col">
	  <?php if ('report' == $task): ?>
	    <h1>ALL Students</h1>
	    <?php reportGenerate($_SESSION['role']); ?>
	  <?php endif; ?>
	</div>
	<div class="col">
	  <?php if ('add' == $task || 'edit' == $task): ?>
	    <?php if ('add' == $task): ?>
	      <h1>ADD New Student</h1>
	    <?php else: ?>
	      <h1>Update Student</h1>
	    <?php endif; ?>
	    <?php if ('edit' == $task): ?>
	      <form method="POST" action="index.php?task=edit">
		<input name="id" type="hidden"
		       value="<?= $_GET['id']??$isAddedOrData['id']; ?>" />
	    <?php else: ?>
		<form method="POST" action="index.php?task=add">
	    <?php endif; ?>
	    <label for="fname">First Name: </label>
	    <input name="fname" type="text"
		   value="<?= $isAddedOrData['fname']??''; ?>" required />
	    <label for="lname">Last Name: </label>
	    <input name="lname" type="text"
		   value="<?= $isAddedOrData['lname']??''; ?>" required />
	    <label for="age">Age: </label>
	    <input name="age" type="number"
		   value="<?= $isAddedOrData['age']??''; ?>" required />
	    <label for="roll">Roll: </label>
	    <input name="roll" type="number"
		   value="<?= $isAddedOrData['roll']??''; ?>" required />
	    <?php if ('add' == $task): ?>
	      <input name="submit" type="submit" value="ADD"/>
	    <?php else: ?>
	      <input name="update" type="submit" value="UPDATE"/>
	    <?php endif; ?>
		</form>
	  <?php endif; ?>
	</div>
	<div class="col">
	  <?php if('login' == $task): ?>
	  <?php include_once ("inc/templates/login.php") ?>
	  <?php endif; ?>
	</div>
      </div>
    </div>
    <script src="assets/js/scripts.js" ></script>
  </body>
</html>

let name;
let chatText;
let chatBody;
(function($){
  $(document).ready(function(){
    name = prompt("What is your name?");
  });
  chatText = $("#chat-text");
  chatText.focus();
  chatText.on('keypress', function(e){
    // console.log();
    if(e.charCode === 13){
      let msg = chatText.val();
      chatText.val('');
      $.post('data.php', {message:msg, sender:name}, function(data){
	// console.log(data);
	chatBody = $(".chat-body");
	chatBody.html(data);
	chatBody.scrollTop(chatBody.prop("scrollHeight"));
      });
      return false;
    }
  });

  setInterval(function(){
    $.post('data.php', {referesh:1}, function(data){
	// console.log(data);
      chatBody = $(".chat-body");
      chatBody.html(data);
      chatBody.scrollTop(chatBody.prop("scrollHeight"));
    });
  }, 3000);
})(jQuery);

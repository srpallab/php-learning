<?php  
$publicKey = "abcdefghijklmnopqrstuvwxyz1234567890";
$mode = $_GET['task']??'encode';
if ('key' == $mode) {
  $keyArray = str_split($publicKey);
  shuffle($keyArray);
  $privateKey = join("", $keyArray);
}else if('decode' == $mode){
  if(isset($_GET['key'])){
    $privateKey = $_GET['key'];
    $mode = 'decode';
  }
}else if('encode' == $mode){
  if(isset($_GET['key'])){
    $privateKey = $_GET['key'];
    $mode = 'encode';
  }
}

if(isset($_POST['send'])){
  if(!empty($_POST['data']) and 'encode' == $_GET['task']) {
    $data = $_POST['data'];
    $count = strlen($data);
    $privateKey = $_POST['key'];
    $newData = "";
    for($i = 0; $i < $count; $i++){
      $position = strpos($privateKey, $data[$i]);
      if ($position !== false) {
        $newData .= $publicKey[$position];
      }else{
        $newData .= $data[$i];
      }
    }
  }else if (!empty($_POST['data']) and 'decode' == $_GET['task']){
    $data = $_POST['data'];
    $count = strlen($data);
    $privateKey = $_POST['key'];
    $newData = "";
    for($i = 0; $i < $count; $i++){
      $position = strpos($publicKey, $data[$i]);
      if ($position !== false) {
        $newData .= $privateKey[$position];
      }else{
        $newData .= $data[$i];
      }
    }
  }
}

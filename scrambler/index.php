<?php include_once "functions.php" ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8"/>
    <title>Data Scrambler</title>
    <link href="style.css" rel="stylesheet"/>
  </head>
  <body>
    <h1>Data Scrambler</h1>
    <p>Use this application to scramble data.</p>
    <div>
      <a href="index.php?task=encode&key=<?= $privateKey ?>">Encode</a>
      <a href="index.php?task=decode&key=<?= $privateKey ?>">Decode</a>
      <a href="index.php?task=key">Generate Key</a>
    </div>
    
    <div class="tpad-10">
      <form action="index.php?task=<?= $mode ?>" method="POST">
        <label for="key">Private Key:</label><br/>
	<input name="key" type="text"
	       id="key" value=<?= $privateKey??$publicKey ?> ><br/>
	<label for="data">Data: </label><br/>
	<textarea cols="80" name="data"
		  rows="10" id="data" ><?= $data??'' ?></textarea><br/>
	<label for="result">Result:</label><br/>
	<textarea cols="80" name="result"
		  rows="10" id="result"><?= $newData??'' ?></textarea><br/>
	<input name="send" type="submit" value="SEND"/>
      </form>
    </div>
  </body>
</html>

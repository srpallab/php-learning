<?php
include_once "config.php";
$myCon = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

if (!$myCon) {
  echo "Failed to Connect MySQL";
  die();
}

if (isset($_POST['task'])) {
    if (!empty($_POST['detail']) && !empty($_POST['time'])) {
        $detail = $_POST['detail'];
        $time = $_POST['time'];
        $sql = "INSERT INTO ".DB_TABLE." (task, t_date) ";
        $sql .= "VALUES ('{$detail}', '{$time}')";
        // echo $sql;
        // die();
        mysqli_query($myCon, $sql);
        header("Location: index.php?add=true");
    }else{
        header("Location: index.php?add=false");
    }
}

if (isset($_POST['taskids'])) {
    $idsString = join(",", $_POST['taskids']);
    if ('del' == $_POST['action']) {
        $sql = "DELETE FROM tasks WHERE id in ({$idsString})";
        mysqli_query($myCon, $sql);
    } elseif ('complete' == $_POST['action']) {
        $sql = "UPDATE tasks SET complete = 1 WHERE id in ({$idsString})";
        mysqli_query($myCon, $sql);
    }
    header("Location: index.php");
}

if (isset($_POST['taskID'])) {
    $taskID = $_POST['taskID'];
    if (isset($_POST['action']) && 'incomplete' == $_POST['action']) {
        $sql = "UPDATE tasks SET complete = 0 WHERE id={$taskID} LIMIT 1";
        mysqli_query($myCon, $sql);
    } elseif (isset($_POST['action']) && 'delete' == $_POST['action']) {
        $sql = "DELETE FROM tasks WHERE id={$taskID} LIMIT 1";
        mysqli_query($myCon, $sql);
    }
    header("Location: index.php");
}
?>

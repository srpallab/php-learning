<?php
require_once('data.php');
$sqlCom = "SELECT * FROM tasks WHERE complete = 1";
$sqlInCom = "SELECT * FROM tasks WHERE complete = 0";
$resultCom = mysqli_query($myCon, $sqlCom);
$resultInCom = mysqli_query($myCon, $sqlInCom);
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8"/>
    <title>Document</title>
    <style type="text/css" media="screen">
     .row{
       padding-bottom: 10px;
       text-align: center;
     }
     table{
       width: 100%;
     }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="row">
	<h1>Task Manager</h1>
	<p>
	  This is a sample task managment project using PHP, MySQL, Javascript.
	</p>
      </div>
      <div class="row">
	<h4>All Completed Tasks</h4>
	<?php if(mysqli_num_rows($resultCom) == 0): ?>
	  <h5>No Tasks Found. Do Some Work.</h5>
	<?php else: ?>
	  <table>
	    <thead>
	      <tr>
		<th>ID</th>
		<th>Task</th>
		<th>Date</th>
		<th>Actions</th>
	      </tr>
	    </thead>
	    <tbody>
	      <?php
	      while($data = mysqli_fetch_assoc($resultCom)):
	      $timestamp = strtotime($data['t_date']);
	      $tDate = date("jS M, Y", $timestamp);
	      ?>
	      <tr>
		<td><?= $data['id']; ?></td>
		<td><?= $data['task']; ?></td>
		<td><?= $tDate; ?></td>
		<td>
		  <button class="incomplete" data-taskid="<?= $data['id']; ?>">
		    Mark as Incomplete
		  </button>
		</td>
	      </tr>
	      <?php endwhile; ?>
	    </tbody>
	  </table>
	  <?php endif; ?>
      </div>
      <div class="row">
	<form method="POST" action="data.php" id="hiddenFrom">
	  <input name="taskID" type="hidden" value="" id="taskID" />
	  <input name="action" type="hidden" value="" id="action" />
	</form>
      </div>
      <div class="row">
	<h4>All Incomplete Tasks </h4>
	<?php if(mysqli_num_rows($resultInCom) == 0): ?>
	  <h5>No Tasks Found. Add New.</h5>
	<?php else: ?>
	  <form action="data.php" method="POST">
	    <table>
	      <thead>
		<tr>
		  <th></th>
		  <th>ID</th>
		  <th>Task</th>
		  <th>Date</th>
		  <th>Actions</th>
		</tr>
	      </thead>
	      <tbody>
		<?php
		while($data = mysqli_fetch_assoc($resultInCom)):
		$timestamp = strtotime($data['t_date']);
		$tDate = date("jS M, Y", $timestamp);
		?>
		  <tr>
		    <td>
		      <input name="taskids[]"
			     type="checkbox"
			     value="<?= $data['id']; ?>"/>
		    </td>
		    <td><?= $data['id']; ?></td>
		    <td><?= $data['task']; ?></td>
		    <td><?= $tDate; ?></td>
		    <td>
		      <a href="#"
			 class="delete" data-taskid="<?= $data['id']; ?>">
			Delete
		      </a>
		    </td>
		  </tr>
		<?php endwhile; mysqli_close($myCon); ?>
	      </tbody>
	    </table>
	    <select name="action">
	      <option value="0">Select an action</option>
	      <option value="del">Delete</option>
	      <option value="complete">Mark as Complete</option>
	    </select>
	    <input name="" type="submit" value="Submit"/>
	  </form>
	<?php endif; ?>
      </div> <!-- row table ends here -->
      <div class="row">
	<form method="POST" action="data.php">
	  <fieldset>
	    <input name="task" type="hidden" value="add"/>
	    <label for="">Task</label>
	    <input name="detail"
		   type="text" value="" placeholder="Task Details" />
	    <label for="">Date</label>
	    <input name="time" type="text" value="" placeholder="Task Date" />
	    <input type="submit" value="ADD TASK"/>
	  </fieldset>
	</form>
      </div> <!-- row task input from ends here -->
    </div> <!-- Container Ends Here -->
    <script src="jquery-3.5.1.slim.min.js"></script>
    <script>
     ;(function ($){
       $(document).ready(function (){
	 // console.log($);
	 $(".incomplete").on("click", function(){
	   var id = $(this).data("taskid");
	   $("#taskID").val(id);
	   $("#action").val("incomplete");
	   $("#hiddenFrom").submit();
	 });
	 $(".delete").on("click", function(){
	   if(confirm("Are you sure?")){
	     console.log("RUM");
	     var id = $(this).data("taskid");
	     $("#taskID").val(id);
	     $("#action").val("delete");
	     $("#hiddenFrom").submit();
	   }
	 });
       });
     })(jQuery);
    </script>
  </body>
</html>

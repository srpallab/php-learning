<?php
session_start();
// DataBase Details
define("DB_HOST", "localhost");
define("DB_NAME", "words");
define("DB_USER", "admin");
define("DB_PASSWORD", 1);

$CONN = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

if (!$CONN) {
  echo "Failed to Connect MySQL";
  die();
}

$dialogCode = 0;

$dialogList = [
    0 =>  '',
    1 => 'Duplicate Email',
    2 => 'Email or Password Incorrect',
    3 => 'New User Created'
];

if (isset($_POST['login'])) {
    $email = $_POST['email'];
    $pwd = $_POST['pwd'];
    $pwdHash = password_hash($pwd, PASSWORD_DEFAULT);
    if ('Login' == $_POST['login']) {
        if ($email && $pwd) {
            $sql = "SELECT * FROM users WHERE email = '{$email}' LIMIT 1";
            $result = mysqli_query($CONN, $sql);
            if ($data = mysqli_fetch_assoc($result)) {
                if (password_verify($pwd, $data['password'])) {
                    $_SESSION['userid'] = $data['id'];
                    header("Location: words.php");
                }else{
                    header("Location: index.php?actionCode=2");
                }
            }
        }
    } elseif ('Register' == $_POST['login']) {
        if ($email && $pwd) {
            $sql = "INSERT INTO users (email, password) VALUES (";
            $sql .= "'{$email}', '{$pwdHash}')";
            mysqli_query($CONN, $sql);
            $error = mysqli_error($CONN);
            if(strpos($error, "Duplicate") !== false){
                header("Location: index.php?actionCode=1");
            }else{
                header("Location: index.php");
            }
        }
    }
}

if (isset($_GET['action'])) {
    $_SESSION['userid'] = null;
    session_destroy();
    header("Location: index.php");
    die();
}



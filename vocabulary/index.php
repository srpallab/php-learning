<?php
require_once("functions.php");
if(isset($_SESSION['userid'])){
  header("Location: words.php");
  die();
}
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8"/>
    <title>Login Or Register</title>
    <link rel="stylesheet" href="style.css" type="text/css" media="screen" />
  </head>
  <body>
    <div class="row">
      <div>
	<a href="#" id="login">Login</a> | 
	<a href="#" id="register">Register</a>
	<h1 id="action-name">Login</h1>
      </div>
      <form action="functions.php" method="POST">
	<label for="email">Email</label>
	<input name="email" type="text" required/>
	<label for="pwd">Password</label>
	<input name="pwd" type="password" required/>
	<p><?= $dialogList[$_GET['actionCode']??0]; ?></p>
	<input name="login" type="submit" value="Login" id="submit-btn" />
      </form>
    </div>
    <script src="jquery-3.5.1.js"></script>
    <script src="main.js"></script>
  </body>
</html>

;(function ($) {
    //console.log($);
    $("#login").on('click', function(){
	$("#submit-btn").val("Login");
	$("#action-name").html("Login");
    });
    $("#register").on('click', function(){
	$("#submit-btn").val("Register");
	$("#action-name").html("Register");
    });
    
    $("#add-words").hide();
    
    $("#word-table").on('click', function(){
	$("#add-words").hide();
	$("#show-words").show();
    });

    $("#word-form").on('click', function(){
	$("#add-words").show();
	$("#show-words").hide();
    });
})(jQuery);

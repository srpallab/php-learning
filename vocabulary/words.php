<?php
require_once("functions.php");
if(!isset($_SESSION['userid'])){
  header("Location: index.php");
  die();
}

?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8"/>
    <title>Vocabulary</title>
    <link rel="stylesheet" href="style.css" type="text/css" media="screen" />
  </head>
  <body>
    <div class="container">
      <div class="side-menu">
	<a href="#" id="word-table">All Words</a>
	<a href="#" id="word-form">Add Word</a>
	<a href="functions.php?action=logout">Logout</a>
      </div>
      <div class="main-section">
	<div class="row" id="show-words">
	  <table>
	    <thead>
	      <th>ID</th>
	      <th>Word</th>
	      <th>Meaning</th>
	    </thead>
	    <tbody>
	      
	    </tbody>
	  </table>
	</div> <!-- Row All Words -->
	<div class="row" id="add-words">
	  <form action="">
	    <label for="word">Word</label>
	    <input name="word" type="text" value=""/>
	    <label for="meaning">Meaning</label>
	    <textarea cols="30" id="" name="meaning" rows="10"></textarea>
	    <input name="add" type="submit" value="Add" />
	  </form>
	</div> <!-- Row Add Word -->
      </div> <!-- Main Container End -->
    </div>
    <script src="jquery-3.5.1.js"></script>
    <script src="main.js"></script>
  </body>
</html>
